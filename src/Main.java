import Agents.CarAgent;
import Objects.ParkingLot;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws StaleProxyException, InterruptedException {

        System.out.println("Create list of parkings");

        ParkingLot parkingLot1 = new ParkingLot("Ace Parking", 5, 20, 15, 5,
                true, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot2 = new ParkingLot("Anytime Parking", 4, 10, 1, 9,
                false, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot3 = new ParkingLot("Bay Parking", 6, 30, 25, 5,
                false, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot4 = new ParkingLot("Central Parking", 7, 50, 5, 45,
                false, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot5 = new ParkingLot("Civic Parking", 5, 20, 15, 5,
                false, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot6 = new ParkingLot("Central Parking", 20, 10, 3, 7,
                false, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot7 = new ParkingLot("Crown Parking", 3, 40, 30, 10,
                false, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot8 = new ParkingLot("Diamond Parking", 4, 15, 5, 10,
                false, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot9 = new ParkingLot("Easy Parking", 7, 8, 2, 6,
                false, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot10 = new ParkingLot("Elite Parking", 10, 10, 5, 5,
                false, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot11 = new ParkingLot("Executive Parking", 6, 20, 12, 8,
                false, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot12 = new ParkingLot("Harmony Parking", 5, 14, 4, 10,
                true, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot13 = new ParkingLot("High-Tech Parking", 5, 10, 5, 5,
                true, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot14 = new ParkingLot("Horizon Parking", 5, 20, 8, 12,
                false, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot15 = new ParkingLot("Imperial Parking", 4, 20, 10, 10,
                true, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot16 = new ParkingLot("King Parking", 10, 10, 5, 5,
                true, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot17 = new ParkingLot("Master Parking", 15, 18, 4, 14,
                false, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot18 = new ParkingLot("Millennium Parking", 12, 8, 1, 0,
                true, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot19 = new ParkingLot("Modern Parking", 3, 20, 15, 5,
                false, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        ParkingLot parkingLot20 = new ParkingLot("National Parking", 2, 20, 15, 5,
                true, new BigDecimal(10.05422), new BigDecimal(10.236589),
                new ArrayList<CarAgent>(), new ArrayList<CarAgent>());

        List<ParkingLot> parkingLots = new ArrayList<>();

        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        parkingLots.add(parkingLot3);
        parkingLots.add(parkingLot4);
        parkingLots.add(parkingLot5);
        parkingLots.add(parkingLot6);
        parkingLots.add(parkingLot7);
        parkingLots.add(parkingLot8);
        parkingLots.add(parkingLot9);
        parkingLots.add(parkingLot10);
        parkingLots.add(parkingLot11);
        parkingLots.add(parkingLot12);
        parkingLots.add(parkingLot13);
        parkingLots.add(parkingLot14);
        parkingLots.add(parkingLot15);
        parkingLots.add(parkingLot16);
        parkingLots.add(parkingLot17);
        parkingLots.add(parkingLot18);
        parkingLots.add(parkingLot19);
        parkingLots.add(parkingLot20);

        final Runtime runTime = Runtime.instance();
        runTime.setCloseVM(true);
        Profile mainProfile = new ProfileImpl(true);
        AgentContainer mainContainer = runTime.createMainContainer(mainProfile);
        AgentController rma = mainContainer.createNewAgent("rma", "jade.tools.rma.rma", null);

        final int[] counter = {1};
        parkingLots.forEach(parkingLot -> {
            AgentController agent;
            try {
                agent = mainContainer.createNewAgent("Parking" + counter[0], "Agents.ParkingAgent", new Object[]{parkingLot});
                agent.start();
                System.out.println("Starting up " + agent.getName() + "...");
                counter[0]++;
                Thread.sleep(900);
            } catch (StaleProxyException | InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        rma.start();
        Thread.sleep(900);
    }
}
