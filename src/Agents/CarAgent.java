package Agents;

import Objects.CarGui;
import Objects.Driver;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

public class CarAgent extends Agent {
    private CarGui carGui;
    private Driver driver;
    private AID[] parkingManagers;

    protected void setup() {
        // Create and show the GUI
        carGui = new CarGui(this);
        carGui.show();
    }

    public void searchForParking(Driver dr) {
        addBehaviour(new TickerBehaviour(this, 30000) {
            protected void onTick() {
                System.out.println("Trying find available parking for " + dr.getDriverName() + " with car " + driver.getMark());
                // Update the list of seller agents
                DFAgentDescription template = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType("book-selling");
                template.addServices(sd);
                try {
                    DFAgentDescription[] result = DFService.search(myAgent, template);
                    System.out.println("Found the following manager agents:");
                    parkingManagers = new AID[result.length];
                    for (int i = 0; i < result.length; ++i) {
                        parkingManagers[i] = result[i].getName();
                        System.out.println(parkingManagers[i].getName());
                    }
                } catch (FIPAException fe) {
                    fe.printStackTrace();
                }

                // Perform the request
//                myAgent.addBehaviour(new RequestPerformer());
            }
        });
    }

}
