package Objects;

import Agents.CarAgent;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class CarGui extends JFrame {
    //TODO Add css to jframe

    private CarAgent carAgent;
    private JLabel markLabel, fullNameLabel, hoursLabel;
    private JTextField markTextField, fullNameTextField, hoursTextField;
    private JCheckBox chargedCheckBox;
    private JButton submitButton;

    public CarGui(CarAgent ca) {
        super(ca.getLocalName());
        carAgent = ca;
        setTitle("Parking form");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(500, 300));

        JPanel formPanel = new JPanel();
        formPanel.setBorder(new EmptyBorder(4, 4, 4, 4));
        formPanel.setLayout(new GridLayout(5, 2));

        markLabel = new JLabel("Car mark:");
        formPanel.add(markLabel);
        markTextField = new JTextField(15);
        formPanel.add(markTextField);

        fullNameLabel = new JLabel("Full name:");
        formPanel.add(fullNameLabel);
        fullNameTextField = new JTextField(15);
        formPanel.add(fullNameTextField);

        hoursLabel = new JLabel("Parking hours:");
        formPanel.add(hoursLabel);
        hoursTextField = new JTextField(15);
        formPanel.add(hoursTextField);

        chargedCheckBox = new JCheckBox("Do you want to charge your car?");
        formPanel.add(chargedCheckBox);

        JPanel buttonPanel = new JPanel();
        submitButton = new JButton("Submit");
        buttonPanel.add(submitButton);


        buttonPanel.add(submitButton);
        getContentPane().add(formPanel, BorderLayout.CENTER);
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);

//        Dialog frame
        JFrame dialogFrame = new JFrame("frame");
        JDialog dialog = new JDialog(dialogFrame, "dialog Box");
        dialog.pack();
        dialog.setLocationRelativeTo(null);

        // create a label
        JLabel dialogLabel = new JLabel("You must fill all the fields");

        // create a button
        JButton dialogButton = new JButton("OK");

        // create a panel
        JPanel dialogPanel = new JPanel();

        dialogPanel.add(dialogLabel);
        dialogPanel.add(dialogButton);

        // add panel to dialog
        dialog.add(dialogPanel);

        // setsize of dialog
        dialog.setSize(200, 150);

        dialogButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);
            }
        });

        submitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                try {
                    if (!markTextField.getText().isEmpty() && !fullNameTextField.getText().isEmpty() &&
                            !hoursTextField.getText().isEmpty()) {
                        String mark = markTextField.getText().trim();
                        String driverName = fullNameTextField.getText().trim();
                        LocalDateTime dateIn = LocalDateTime.now();
                        LocalDateTime dateOut = dateIn.plusMinutes(Long.parseLong(hoursTextField.getText().trim()));
                        BigDecimal latitude = new BigDecimal((Math.random() * 100) - 50);
                        BigDecimal longitude = new BigDecimal((Math.random() * 100) - 50);
                        Driver driver = new Driver(mark, driverName, chargedCheckBox.isSelected(), dateIn, dateOut,
                                latitude, longitude);
                        System.out.println("Driver " + driver.getDriverName() + " with car " + driver.getMark()
                                + " is looking for parking!");
                        ca.searchForParking(driver);
                        markTextField.setText("");
                        fullNameTextField.setText("");
                        hoursTextField.setText("");
                    } else {
                        dialog.setVisible(true);
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(CarGui.this, "Invalid values. " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        pack();
        setLocationRelativeTo(null);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                carAgent.doDelete();
            }
        });
        setVisible(true);
    }
}
