package Objects;

import Agents.CarAgent;
import jade.core.AID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ParkingLot {
    private String name;
    private Integer pricePerHour;
    private int capacity;
    private int availableSpace;
    private int reservedSpace;
    private boolean acceptElectricCars;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private List<CarAgent> accepted = new ArrayList<>();
    private List<CarAgent> parked = new ArrayList<>();
    private AID agentID;


    public ParkingLot(String name, Integer pricePerHour, int capacity, int availableSpace, int reservedSpace
            , boolean acceptElectricCars, BigDecimal latitude, BigDecimal longitude,
                      List<CarAgent> accepted, List<CarAgent> parked) {
        this.name = name;
        this.pricePerHour = pricePerHour;
        this.capacity = capacity;
        this.availableSpace = availableSpace;
        this.reservedSpace = reservedSpace;
        this.acceptElectricCars = acceptElectricCars;
        this.latitude = latitude;
        this.longitude = longitude;
        this.accepted = accepted;
        this.parked = parked;
//        this.agentID = agentID;
    }
}
