package Objects;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Driver {
    private String mark;
    private String driverName;
    private boolean isForCharged;
    private LocalDateTime dateIn;
    private LocalDateTime dateOut;
    private BigDecimal latitude;
    private BigDecimal longitude;

    public Driver(String mark, String driverName, boolean isForCharged,
                  LocalDateTime dateIn, LocalDateTime dateOut, BigDecimal latitude,
                  BigDecimal longitude) {
        this.mark = mark;
        this.driverName = driverName;
        this.isForCharged = isForCharged;
        this.dateIn = dateIn;
        this.dateOut = dateOut;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public boolean isForCharged() {
        return isForCharged;
    }

    public void setForCharged(boolean forCharged) {
        isForCharged = forCharged;
    }

    public LocalDateTime getDateIn() {
        return dateIn;
    }

    public void setDateIn(LocalDateTime dateIn) {
        this.dateIn = dateIn;
    }

    public LocalDateTime getDateOut() {
        return dateOut;
    }

    public void setDateOut(LocalDateTime dateOut) {
        this.dateOut = dateOut;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }
}
